#!/bin/bash
apt update 

echo "[TASK 1] Clone Repo"
git clone https://gitlab.com/yashar-ansary/openstack.git /opt/openstack-ansible
cd /opt/openstack-ansible/
git checkout stable/wallaby

echo "[TASK 2] Bootstrap Deployment Host"
./scripts/bootstrap-ansible.sh


apt install sshpass -y 
export SSHPASS='5#fXD26qrLq!P@*y'
ssh-keyscan -H 172.29.236.12  >> ~/.ssh/known_hosts
ssh-keyscan -H 172.29.236.13  >> ~/.ssh/known_hosts
ssh-keyscan -H 172.29.236.21  >> ~/.ssh/known_hosts
ssh-keyscan -H 172.29.236.22  >> ~/.ssh/known_hosts

echo "[TASK 3] Applying Network Configurations"
rm -rf /etc/netplan/*
cp /opt/openstack-ansible/etc/netplan/01-netcfg.yaml /etc/netplan/
cp /opt/openstack-ansible/etc/netplan/infra1.yaml /etc/netplan/
netplan apply

sshpass -e ssh-copy-id root@172.29.236.12
ssh 172.29.236.12 "rm -rf /etc/netplan/*"
scp /opt/openstack-ansible/etc/netplan/01-netcfg.yaml 172.29.236.12:/etc/netplan/
scp /opt/openstack-ansible/etc/netplan/infra2.yaml 172.29.236.12:/etc/netplan/
ssh 172.29.236.12 "netplan apply"

sshpass -e ssh-copy-id root@172.29.236.13
ssh 172.29.236.13 "rm -rf /etc/netplan/*"
scp /opt/openstack-ansible/etc/netplan/01-netcfg.yaml 172.29.236.13:/etc/netplan/ 
scp /opt/openstack-ansible/etc/netplan/infra3.yaml 172.29.236.13:/etc/netplan/
ssh 172.29.236.13 "netplan apply"

sshpass -e ssh-copy-id root@172.29.236.21
ssh 172.29.236.21 "rm -rf /etc/netplan/*"
scp /opt/openstack-ansible/etc/netplan/01-netcfg.yaml 172.29.236.21:/etc/netplan/
scp /opt/openstack-ansible/etc/netplan/osd1.yaml 172.29.236.21:/etc/netplan/
ssh 172.29.236.21 "netplan apply"

sshpass -e ssh-copy-id root@172.29.236.22
ssh 172.29.236.22 "rm -rf /etc/netplan/*"
scp /opt/openstack-ansible/etc/netplan/01-netcfg.yaml 172.29.236.22:/etc/netplan/
scp /opt/openstack-ansible/etc/netplan/osd2.yaml 172.29.236.22:/etc/netplan/
ssh 172.29.236.22 "netplan apply"

echo "[TASK 4] User Secret Gen"
cp -a /opt/openstack-ansible/etc/openstack_deploy/ /etc/
python3 scripts/pw-token-gen.py --file /etc/openstack_deploy/user_secrets.yml

 
echo "[TASK ] Run Openstack PlayBooks"
cd /opt/openstack-ansible/playbooks/
openstack-ansible setup-hosts.yml
openstack-ansible haproxy-install.yml
openstack-ansible ceph-install.yml
openstack-ansible setup-infrastructure.yml
openstack-ansible setup-openstack.yml